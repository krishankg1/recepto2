from django.contrib import admin
from . models import Places, accounts
from . models import Project
from . models import Lot
from . models import Csv_Data
from . models import Data
from . models import CompanyAddress

# Register your models here.

admin.site.register(Places)
admin.site.register(Project)
admin.site.register(Lot)
admin.site.register(Csv_Data)
admin.site.register(Data)
admin.site.register(CompanyAddress)
