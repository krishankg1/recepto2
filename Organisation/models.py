from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.conf import settings
from django.contrib.auth.models import User
import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw
from django.db.models.signals import post_save
from django.dispatch import receiver



class MyAccountManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user._set_pk_val(username)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user._set_pk_val(username)
        user.save(using=self._db)
        return user


class accounts(AbstractBaseUser):
    organization = models.CharField(verbose_name='organization', max_length=100, default=" ")
    email = models.EmailField(verbose_name="email", max_length=60, unique=True, default=" ")
    username = models.CharField(verbose_name='username', primary_key=True, max_length=30, unique=True, default=" ")
    is_admin = models.BooleanField(default=False)
    last_login = models.TimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    domain = models.CharField(max_length=40, default=' ')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = MyAccountManager()


    def __str__(self):
        return self.email

    # For checking permissions. to keep it simple all admin have ALL permissons
    def has_perm(self, perm, obj=None):
        return self.is_admin

    # Does this user have permission to view this app? (ALWAYS YES FOR SIMPLICITY)
    def has_module_perms(self, app_label):
        return True






class Places(models.Model):
    organization = models.CharField(default=" ", max_length=100, null=False, blank=False)
    placename = models.CharField(max_length=200, null=False, blank=False, unique=True)
    distt = models.CharField(max_length=12, null=False, blank=False)
    state = models.CharField(max_length=20, null=False, blank=False)
    company_data = models.JSONField(null=True, blank=True)
    username = models.ForeignKey(accounts, related_name='places', on_delete=models.CASCADE, max_length=20, null=False, blank=False, default='personname')
    qr_code = models.ImageField(upload_to='qr_code', blank=True)
    ur = models.CharField(max_length=200, default='http://127.0.0.1:8000/visitor/visitor/')
    @property
    def combine(self):
        return '{}-{}-{}'.format(self.ur, self.organization, self.placename)

    url = combine




    def __str__(self):
        return self.organization

    def __str__(self):
        return str(self.placename)

    def save(self, *args, **kwargs):
        qrcode_img = qrcode.make(self.url)
        canvas = Image.new('RGB', (450, 450), 'white')
        draw = ImageDraw.Draw(canvas)
        canvas.paste(qrcode_img)
        fname = f'qr_code-{self.placename}.png'
        buffer = BytesIO()
        canvas.save(buffer, 'PNG')
        self.qr_code.save(fname, File(buffer), save=False)
        canvas.close()
        super().save(*args, **kwargs)


class Project(models.Model):
    id = models.AutoField(primary_key=True)
    project_name = models.CharField(max_length=100, null=False, blank=False)
    # lot_id = models.PositiveIntegerField()
    # project_id = models.IntegerField()
    image = models.ImageField(
        upload_to="image/", null=True,
        blank=True,)

    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_on = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return f"{self.id}"
        # return self.project_name


class Lot(models.Model):
    id = models.AutoField(primary_key=True)
    lot_name = models.CharField(max_length=100, null=False, blank=False)
    project_id = models.ForeignKey(
        Project, related_name="project",
        on_delete=models.DO_NOTHING, null=True, blank=True,
    )
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_on = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return f"{self.id}"
        # return self.project_name


class Csv_Data(models.Model):
    id = models.AutoField(primary_key=True)
    month_name = models.CharField(max_length=100, null=True, blank=True)

    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_on = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return f"{self.id}"
        # return self.project_name


class Data(models.Model):
    make = models.CharField(max_length=200, null=True, blank=True)
    family = models.CharField(max_length=200, null=True, blank=True)
    varient = models.CharField(max_length=200, null=True, blank=True)
    new_pr = models.CharField(max_length=200, null=True, blank=True)
    nvic = models.CharField(max_length=200, null=True, blank=True)
    year = models.CharField(max_length=20, null=True, blank=True)
    image = models.ImageField(
        upload_to="media/image/", null=True,
        blank=True, )

    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.make

class CompanyAddress(models.Model):
    abn_number = models.CharField(max_length=200, null=False, blank=False, unique=True)
    acn_number = models.CharField(max_length=200, null=True, blank=True)
    company_data = models.JSONField(null=True, blank=True)
    api_hit_date = models.DateTimeField(null=True, blank=True)
    # company_data = JSONField(null=True, blank=True)

    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified_on = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.abn_number






