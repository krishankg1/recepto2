import datetime
import time

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.generics import RetrieveAPIView
from . models import Places, accounts
from . models import Project
from . models import Lot
from . models import CompanyAddress
from . models import Data
from . models import Csv_Data
from . serializers import PlacesSerializer, accountsSerializer, acountsSerializercreate, accountsSerializerupdate, loginserializer, PlacesSerializer, PlacesSerializercreate, PlacesSerializerupdate
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from . serializers import ProjectTypeListSerializers
from . serializers import LotTypeListSerializers
from . serializers import CompanyAddressCheckSerializers
from . serializers import LotCreateSerializer
from . serializers import UploadSerializer
from rest_framework import (filters, generics, exceptions,
                            response, status, views, viewsets)
from django.http import HttpResponse
from django.http import Http404

from channels.generic.websocket import AsyncJsonWebsocketConsumer


class PlaceViewset(viewsets.ModelViewSet):
    serializer_class = PlacesSerializer
    queryset = Places.objects.all()
    # permission_classes = (IsAuthenticated,)
    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(username=self.request.user)
        return query_set

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return PlacesSerializer
        if self.request.method == 'POST':
            return PlacesSerializercreate

    def delete(self, request, *args, **kwargs):
        username = request.data['username']
        id = request.data['id']
        instance = Places.objects.get(username=username, id=id)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, *args, **kwargs):
        id = request.data['id']
        place = Places.objects.get(id=id)

        data = request.data
        place.id = data.get("id", place.id)
        place.organization = data.get("organization", place.organization)
        place.placename = data.get("placename", place.placename)
        place.distt = data.get("distt", place.distt)
        place.state = data.get("state", place.state)

        place.save()

        serializer = PlacesSerializerupdate(place)
        return Response(serializer.data)


class accountsViewset(viewsets.ModelViewSet):
    serializer_class = accountsSerializer
    queryset = accounts.objects.all()


    def get_serializer_class(self):
        if self.request.method == 'GET':
            return accountsSerializer
        if self.request.method == 'POST':
            return acountsSerializercreate
        else:
            return accountsSerializer

    def put(self, request, *args, **kwargs):
        username = request.data['username']
        account = accounts.objects.get(username=username)

        data = request.data

        account.organization = data.get("organization", account.organization)
        account.email = data.get("email", account.email)
        account.username = data.get("username", account.username)


        account.save()

        serializer = accountsSerializerupdate(account)
        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        username = request.data['username']
        instance = accounts.objects.get(username=username)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




class LoginView(RetrieveAPIView):

    permission_classes = (AllowAny,)
    serializer_class = loginserializer


    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        print(serializer, "aaaaaaaaaaaaaaaaaaaaaaaa")
        if serializer.is_valid(raise_exception=True):
            response = {
                       'success' : 'True',
                'status code' : status.HTTP_200_OK,
            'message': 'User logged in  successfully',
                'token': serializer.data['token'],
                'email': serializer.data['email'],

            }
        status_code = status.HTTP_200_OK

        return Response(response, status=status_code)



class ProjectViewSet(viewsets.ModelViewSet):
    """ items model view """

    model = Project
    # pagination_class = pagination.RestAPIPagination

    # filter_backends = [
    #     django_filters.rest_framework.DjangoFilterBackend,
    #     filters.OrderingFilter
    # ]

    ordering_fields = ['created_on', ]
    ordering = ['-created_on']
    # filter_class = custom_filters.ItemsFilterClass

    def get_queryset(self):
        return self.model.objects.all()

    def get_serializer_class(self):

        if self.request.method == 'GET' and not self.kwargs.get('pk'):
            return ProjectTypeListSerializers
        # if self.request.method in ['POST', 'PATCH', 'PUT']:
        #     return serializers.ItemsCreateSerializer
        # else:
        #     return serializers.ItemsListSerializer

    def list(self, request, *args, **kwargs):
        """ custom list method """
        remove_pagination = request.query_params.get('remove_pagination', None)
        if remove_pagination:
            self.pagination_class = None

        return super(ProjectViewSet, self).list(request, *args, **kwargs)

    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     if serializer.is_valid(raise_exception=True):
    #         self.perform_create(serializer)
    #         data = serializers.ItemsListSerializer(serializer.instance).data
    #         return response.Response(data, status=status.HTTP_201_CREATED)
    #
    # def destroy(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     instance.set_inactive()
    #     instance.save()
    #     return response.Response(status=status.HTTP_204_NO_CONTENT)


class LottViewSet(viewsets.ModelViewSet):
    """ items model view """

    model = Lot
    # pagination_class = pagination.RestAPIPagination

    # filter_backends = [
    #     django_filters.rest_framework.DjangoFilterBackend,
    #     filters.OrderingFilter
    # ]

    ordering_fields = ['created_on', ]
    ordering = ['-created_on']
    # filter_class = custom_filters.ItemsFilterClass

    def get_queryset(self):
        return self.model.objects.all()

    def get_serializer_class(self):

        if self.request.method == 'GET' and not self.kwargs.get('pk'):
            return LotTypeListSerializers
        if self.request.method in ['POST', 'PATCH', 'PUT']:
            return LotCreateSerializer
        # else:
        #     return serializers.ItemsListSerializer

    def list(self, request, *args, **kwargs):
        """ custom list method """
        remove_pagination = request.query_params.get('remove_pagination', None)
        if remove_pagination:
            self.pagination_class = None

        return super(LottViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            self.perform_create(serializer)
            print(serializer.instance.lot_name)
            data = LotTypeListSerializers(serializer.instance).data
            return response.Response(data, status=status.HTTP_201_CREATED)

    # def destroy(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     instance.set_inactive()
    #     instance.save()
    #     return response.Response(status=status.HTTP_204_NO_CONTENT)


# class DataUploadAPIView(generics.CreateAPIView):
#     """POST:API to send notifications"""
#
#     serializer_class = UploadSerializer
#
#     def post(self, request, *args, **kwargs):
#         from zipfile import ZipFile
#         import zipfile
#         from django.conf import settings
#         from PIL import Image
#         import glob
#         import os, os.path
#         import shutil
#         import pathlib
#
#         import ftplib
#         from ftplib import FTP
#         global ftp
#         ftp = FTP('ftp.glassguide.com.au', user='dafs_ftp', passwd='Sefasob@17')
#
#         print(ftp.dir())
#         print(ftp.file)
#
#         # filematch = '*Photo_Jan21.zip*'  # a match for any file in this case, can be changed or left for user to input
#         filematch = '*Photo_Feb21.zip*'
#         # filematch = '*eis.zip*'
#         # filematch = '*Photo*'
#         ftp.cwd("/")
#
#
#         # # filename = 'Photo_Jan21.zip'
#         # filename = 'Photo_Feb21.zip'
#         # # filename = 'Apr21eis.zip'
#         # print(ftp.size(filename))
#         #
#         # # ftp.retrbinary("RETR " + filename, open(str(settings.BASE_DIR) + "/data/" +filename, 'wb').write,22253015)
#         # ftp.quit()
#         #
#         # zf = ZipFile(str(settings.BASE_DIR) + "/data/" +filename, "r")
#         # file = filename.split('.')[0]
#         # zf.extractall(str(settings.BASE_DIR) + '/data/' + file)
#
#         file = pathlib.Path(str(settings.BASE_DIR) + '/data/')
#         if not file.exists():
#             os.mkdir(str(settings.BASE_DIR) + '/data/')
#         for filename in ftp.nlst(filematch):
#             if filename.endswith('.zip'):
#                 print('asasasasas',filename)
#                 size = ftp.size(filename)
#
#                 ftp.retrbinary("RETR " + filename, open(str(settings.BASE_DIR) + "/data/" +filename, 'wb').write, size)
#                 zf = ZipFile(str(settings.BASE_DIR) + "/data/" + filename, "r")
#                 file = filename.split('.')[0]
#                 zf.extractall(str(settings.BASE_DIR) + '/data/' + file)
#
#
#
#
#
#
#         # if request.FILES:
#         #     files = request.FILES['csv_file']
#         #     if not files.name.endswith('.zip'):
#         #         raise Http404
#         # else:
#         #     return HttpResponse("{'msg':'no file detected'}")
#         #
#         # remove_path = str(settings.BASE_DIR) + "/data/"
#         # shutil.rmtree(remove_path, ignore_errors=True)
#         #
#         #
#         # zf = ZipFile(files, "r")
#         # # zf.extractall('/home/jaarvis/Downloads/file')
#         # zf.extractall(str(settings.BASE_DIR) + "/data/")
#         # # zf.extractall(settings.BASE_DIR + '/downloads/' + filename)
#         #
#         # # path = "/home/jaarvis/Downloads/file/Photo_Jan21"
#         # # path = "settings.BASE_DIR + '/data/'"
#         # # print(str(settings.BASE_DIR) + '/data/Photo_Jan21')
#         # # paths = "settings.BASE_DIR) + '/data/'{0}".format('Photo_Jan21')
#         #
#         # file_name = os.listdir(str(settings.BASE_DIR) + '/data/')
#         # # path = os.listdir(str(settings.BASE_DIR) + '/data/'+ file_name)
#         # fileList = os.listdir(str(settings.BASE_DIR) + '/data/'+ file_name[0])
#         # photo_path = str(settings.BASE_DIR) + '/data/{0}'.format(file_name[0])
#         #
#         #
#         # file = pathlib.Path(str(settings.BASE_DIR) + '/media/image')
#         # if not file.exists():
#         #     os.mkdir(str(settings.BASE_DIR) + '/media/image')
#         #
#         # nvic = []
#         # project = Project.objects.all()
#         # for i in project:
#         #     nvic.append(i.project_name)
#         #
#         # for i in fileList:
#         #     photo_nvic = i.split('.')[0]
#         #     exten = i.split('.')[1]
#         #     if photo_nvic in nvic:
#         #         proj_obj = Project.objects.filter(project_name=photo_nvic).first()
#         #         if not proj_obj.image:
#         #             aa = Image.open(os.path.join(photo_path, i))
#         #             filename = i
#         #             aa.save('media/image/{0}'.format(filename))
#         #             image_path ="image/{0}".format(filename)
#         #             proj_obj.image = image_path
#         #             proj_obj.save()
#         #             # proj_obj = Project.objects.filter(project_name=photo_nvic).first()
#         #             # proj_obj.image= i
#         #             # proj_obj.save()
#         #         else:
#         #             continue
#         #
#         #     if photo_nvic not in nvic:
#         #         continue
#         #     if not exten == '.jpg':
#         #         continue
#         #
#         # return HttpResponse('successfully........')
#
#
#
#
#
#
#         # nvic = []
#         # project = Project.objects.all()
#         # for i in project:
#         #     nvic.append(i.project_name)
#         #
#         # files = request.FILES.getlist('csv_file')
#         #
#         # for i in files:
#         #     photo_nvic = i.name.split('.')[0]
#         #     exten = i.name.split('.')[1]
#         #
#         #     if photo_nvic in nvic:
#         #         proj_obj = Project.objects.filter(project_name=photo_nvic).first()
#         #         proj_obj.image= i
#         #         proj_obj.save()
#         #         print('aaaaaaaaaaaa')
#         #
#         #     if photo_nvic not in nvic:
#         #         continue
#         #     if not exten =='.jpg':
#         #         continue
#
#             # project.image=i
#             # project.save()
#             # print(i)
#             # print('aaaaaaaaaa',i.name.startswith('.jpg'))
#             # print('aaaaaaaaaa',i.name.split('.')[0])
#
#             # filename = 'qrcode00{0}.png'.format(user.id)
#             # img.save('media/qr_code/{0}'.format(filename))
#             #
#             # # from django.conf import settings
#             # # imgs = open(settings.BASE_DIR + '/data/' + filename)
#             #
#             # path ="media/qr_code/{0}".format(filename)
#             # user.qr_code = path
#             # user.save()
#
#
#
#         import ftplib
#         #
#         # # #Open ftp connection
#         # # ftp = ftplib.FTP('ftp://ftp.glassguide.com.au/', 'anonymous',
#         # # 'Sefasob@17')
#         #
#         # from ftplib import FTP
#         # from django.conf import settings
#         #
#         #
#         # global ftp
#         # from zipfile import ZipFile
#         # import zipfile
#         #
#         # ftp = FTP('ftp.glassguide.com.au', user='dafs_ftp', passwd='Sefasob@17')
#         # # print(ftp.dir())
#         # # print(ftp.file)
#         #
#         # ftp.cwd("/")
#         # filename = "Feb21eis.zip"
#         # gFile = open("Feb21eis.zip", "wb")
#         #
#         #
#         # filematch = '*Feb21eis.zip*' # a match for any file in this case, can be changed or left for user to input
#         #
#         # ftp.cwd("/")
#         #
#         # for filename in ftp.nlst(filematch): # Loop - looking for matching files
#         #
#         #     # fhandle = open(filename, 'wb')
#         #     path = "/home/jaarvis/Downloads/"+ filename
#         #     fhandle = open(path + filename, 'wb')
#         #     # fhandle = open(filename, 'wb')
#         #     print ('Getting ' + filename)
#         #     ftp.retrbinary('RETR ' + filename, fhandle.write)
#         #
#         #     zf = ZipFile("/home/jaarvis/Downloads/Feb21eis.zip", "r")
#         #     zf.extractall("/home/jaarvis/Downloads/aa")
#         #     # zf = ZipFile("/home/jaarvis/Downloads/Feb21eis.zip", "r")
#         #     # zf = ZipFile(path+filename, "r")
#         #     # zf.extractall(path+'aa')
#         #
#         #     # zf = ZipFile("/home/jaarvis/Downloads/Feb21eis.zip", "r")
#         #     # zf.extractall("/home/jaarvis/Downloads/zz")
#         #     # zf.close()
#         #
#         #     fhandle.close()
#         #
#         #
#         # # zf.extractall(settings.BASE_DIR + '/downloads/' + filename)
#         # # f = open("/home/jaarvis/Downloads/Feb21eis.zip", "r")
#         #
#         #
#         # # zf = ZipFile("/home/jaarvis/Downloads/Feb21eis.zip", "r")
#         # # zf.extractall("/home/jaarvis/Downloads/zz")
#         # # zf.close()
#         #
#         #
#         #
#         #
#         #
#         # # import os
#         # # path = "/home/jaarvis/Downloads/aa/"
#         # # fileList = os.listdir(path)
#         # # for i in fileList:
#         # #     file = open(os.path.join('/home/jaarvis/Downloads/aa/'+ i), 'r')
#         # #     if file.name.endswith('U12'):
#         # #         print(file.name)
#         # #     if file.name.endswith('N12'):
#         # #         print(file.name)
#         #
#         #
#         #
#         # # meter = 2500
#         # # kilometers = meter /1000.0
#         # #
#         # # print(kilometers)
#
#
#
#
#
#
#         #     if not csvfile.name.endswith('.U12') and not csvfile.name.endswith('.N12'):
#         #         raise Http404
#         # else:
#         #     return HttpResponse("{'msg':'no file detected'}")
#         #
#         # # if csvfile.name == 'CCG18051.U12':
#         # #     raise Http404
#         # import os
#         # # with open('/home/jaarvis/Downloads/aa/CVG10100.N12', 'r') as f:
#         # #     data = f.read()
#         # #     print('sssssss', type(data))
#         #
#         # # a=open("/home/jaarvis/Downloads/aa/CCG18021.U12", "w")
#         # # print(a)
#         # # print(type(a))
#         # # path = "/home/jaarvis/Downloads/aa/"
#         # # fileList = os.listdir(path)
#         # # for i in fileList:
#         # #     print(type(i))
#         # #     # if i.endswith('N12'):
#         # #     #     csvfile = i
#         # #
#         # #     # print('aaaaaaa',i.endswith('N12'))
#         # #     file = open(os.path.join('/home/jaarvis/Downloads/aa/'+ i), 'r')
#         # #     if file.name.endswith('U12'):
#         # #         print('aaaaaaaaaaaaaaaaaa',type(file.name))
#         # #         # csvfile = file.read().decode('utf-8')
#         # #         print(file.name)
#         #         # print(file)
#         #     # if file.name.endswith('N12'):
#         #     #     print(file.name)
#         #
#         #
#         # # if request.FILES:
#         # #     csvfile = request.FILES['csv_file']
#         #
#         # csvfile = csvfile.read().decode('utf-8')
#         # # print(csvfile)
#         #
#         #
#         # # filename = 'data1214.csv_file'
#         # # df = pd.read_fwf('/home/jaarvis/Downloads/CVG10100.N12')
#         # # df.to_csv(settings.BASE_DIR + '/downloads/' + filename)
#         # # data = pd.read_csv(settings.BASE_DIR + '/downloads/' + filename)
#         # # print(data)
#         # # for col in data.columns:
#         # #     print(col)
#         #
#         #
#         # filename = 'data_uploads.csv_file'
#         # f = open(settings.BASE_DIR + '/data/' + filename, "w")
#         # f.write(csvfile)
#         # # f.write(data)
#         # f.close()
#         # df = pd.read_fwf(settings.BASE_DIR + '/data/' + filename)
#         # df.to_csv(settings.BASE_DIR + '/data/' + filename)
#         #
#         # # df = pd.read_csv(settings.BASE_DIR + '/data/' + filename)
#         # df = pd.read_csv(settings.BASE_DIR + '/data/' + filename,usecols=lambda c: c in {'MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR'},
#         #                  # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                  )
#         # make_column = pd.read_csv(settings.BASE_DIR + '/data/' + filename,
#         #                  usecols=lambda c: c in {'MAKE',},
#         #                  # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                  )
#         # family_column = pd.read_csv(settings.BASE_DIR + '/data/' + filename,
#         #                           usecols=lambda c: c in {'FAMILY', },
#         #                           # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                           )
#         # variant_column = pd.read_csv(settings.BASE_DIR + '/data/' + filename,
#         #                             usecols=lambda c: c in {'VARIANT', },
#         #                             # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                             )
#         # new_price_column = pd.read_csv(settings.BASE_DIR + '/data/' + filename,
#         #                              usecols=lambda c: c in {'NEW PR', },
#         #                              # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                              )
#         #
#         # nvic_column = pd.read_csv(settings.BASE_DIR + '/data/' + filename,
#         #                                usecols=lambda c: c in {'NVIC', },
#         #                                # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                                )
#         #
#         # year_column = pd.read_csv(settings.BASE_DIR + '/data/' + filename,
#         #                           usecols=lambda c: c in {'YEAR', },
#         #                           # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#         #                           )
#         #
#         # # cols = ["MAKE", "FAMILY", "VARIANT", "NEW PR", "NVIC"]
#         # # data = pandas.read_csv(settings.BASE_DIR + '/data/' + filename, names=cols)
#         # data = pandas.read_csv(settings.BASE_DIR + '/data/' + filename, )
#         # # print(data)
#         # # print('aaaaaaaaaaaaa',len(data))
#         # column_len = len(df.columns)
#         # # print(column_len)
#         #
#         # for i in range(len(data)):
#         #     if i == 0 or i == 1:
#         #         continue
#         #     if column_len == 5:
#         #         make = make_column.values[i][0]
#         #         family = family_column.values[i][0]
#         #         varient = variant_column.values[i][0]
#         #         nvic = nvic_column.values[i][0]
#         #         year = year_column.values[i][0]
#         #         # print('make', make, 'family', family, 'varient', varient,'nvic', nvic, 'year', year)
#         #
#         #         ctx = {
#         #             'make': make,
#         #             'family': family,
#         #             'varient': varient,
#         #             'new_pr': None,
#         #             'nvic': nvic,
#         #             'year': year,
#         #
#         #         }
#         #
#         #         try:
#         #             # print('24')
#         #             task = DataUploadTask()
#         #             task.run(**ctx)
#         #
#         #         except Exception as e:
#         #             return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#         #
#         #     if column_len == 6:
#         #         make = make_column.values[i][0]
#         #         family = family_column.values[i][0]
#         #         varient = variant_column.values[i][0]
#         #         new_pr = new_price_column.values[i][0]
#         #         nvic = nvic_column.values[i][0]
#         #         year = year_column.values[i][0]
#         #
#         #         # make = df.values[i][0]
#         #         # family = df.values[i][1]
#         #         # varient = df.values[i][2]
#         #         # nvic = df.values[i][3]
#         #         # year = df.values[i][4]
#         #         # new_pr = df.values[i][5]
#         #         # print('make', make, 'family', family,'varient', varient,'new_pr', new_pr,'nvic', nvic, 'year', year)
#         #
#         #         ctx = {
#         #             'make': make,
#         #             'family': family,
#         #             'varient': varient,
#         #             'new_pr': new_pr,
#         #             'nvic': nvic,
#         #             'year': year,
#         #
#         #         }
#         #         try:
#         #             # print('24')
#         #             task = DataUploadTask()
#         #             task.run(**ctx)
#         #
#         #         except Exception as e:
#         #             return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#         #
#         #
#         # # if column_len == 24 and csvfiles.name == 'CCG18051.U12':
#         # #     for i in range(len(data)):
#         # #         if i == 0 or i == 1:
#         # #             continue
#         # #         make = df.values[i][3]
#         # #         family = df.values[i][4]
#         # #         varient = df.values[i][5]
#         # #         new_pr = df.values[i][20]
#         # #         nvic = df.values[i][18]
#         # #         year = df.values[i][19]
#         # #         # print('make', make, 'family', family, 'varient', varient, 'new_pr', new_pr, 'nvic', nvic, 'year', year)
#         # #         # print('aaaaaaa')
#         # #         ctx = {
#         # #             'make': make,
#         # #             'family': family,
#         # #             'varient': varient,
#         # #             'new_pr': new_pr,
#         # #             'nvic': nvic,
#         # #             'year': year,
#         # #
#         # #         }
#         # #
#         # #         try:
#         # #             # print('24')
#         # #             task = DataUploadTask()
#         # #             task.run(**ctx)
#         # #
#         # #         except Exception as e:
#         # #             return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#         # #
#         # # if column_len == 21:
#         # #     for i in range(len(data)):
#         # #         if i == 0 or i == 1:
#         # #             continue
#         # #         make = df.values[i][3]
#         # #         family = df.values[i][4]
#         # #         varient = df.values[i][5]
#         # #         new_pr = df.values[i][20]
#         # #         nvic = df.values[i][18]
#         # #         year = df.values[i][19]
#         # #         # print('make', make, 'family', family, 'varient', varient, 'new_pr', new_pr, 'nvic', nvic, 'year', year)
#         # #         # print('make', new_pr)
#         # #         ctx = {
#         # #             'make': make,
#         # #             'family': family,
#         # #             'varient': varient,
#         # #             'new_pr': new_pr,
#         # #             'nvic': nvic,
#         # #             'year': year,
#         # #
#         # #         }
#         # #         # print(ctx)
#         # #         try:
#         # #             task = DataUploadTask()
#         # #             task.run(**ctx)
#         # #
#         # #             # task = task.delay(**ctx)
#         # #             # return response.Response({"notification":task.id}, status=status.HTTP_201_CREATED)
#         # #             # return response.Response({"message":"successfully....."}, status=status.HTTP_201_CREATED)
#         # #
#         # #         except Exception as e:
#         # #             return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#         # #
#         # # if column_len == 24:
#         # #     for i in range(len(data)):
#         # #         if i == 0 or i == 1:
#         # #             continue
#         # #         make = df.values[i][5]
#         # #         family = df.values[i][6]
#         # #         varient = df.values[i][7]
#         # #         new_pr = df.values[i][15]
#         # #         nvic = df.values[i][19]
#         # #         year = df.values[i][20]
#         # #         # print('make', make, 'family', family, 'varient', varient, 'new_pr', new_pr, 'nvic', nvic)
#         # #         # print('aaaaaaa')
#         # #         ctx = {
#         # #             'make': make,
#         # #             'family': family,
#         # #             'varient': varient,
#         # #             'new_pr': new_pr,
#         # #             'nvic': nvic,
#         # #             'year': year,
#         # #
#         # #         }
#         # #
#         # #         # Data.objects.create(**ctx)
#         # #         # task = SendNotificationOnApp()
#         # #         # print(task)
#         # #         # task.run(**ctx)
#         # #         # return response.Response(status=status.HTTP_201_CREATED)
#         # #         try:
#         # #             # print('24')
#         # #             task = DataUploadTask()
#         # #             task.run(**ctx)
#         # #
#         # #             # task = task.delay(**ctx)
#         # #             # return response.Response({"notification":task.id}, status=status.HTTP_201_CREATED)
#         # #             # return response.Response({"message":"successfully....."}, status=status.HTTP_201_CREATED)
#         # #
#         # #         except Exception as e:
#         # #             return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#         # #
#         # # if column_len == 23:
#         # #     for i in range(len(data)):
#         # #         if i==0 or i==1:
#         # #             continue
#         # #         make = df.values[i][5]
#         # #         family = df.values[i][6]
#         # #         varient = df.values[i][7]
#         # #         # new_pr = df.values[i][15]
#         # #         nvic = df.values[i][16]
#         # #         year = df.values[i][17]
#         # #         # print('make', make, 'family', family, 'varient', varient, 'nvic', nvic)
#         # #         ctx = {
#         # #             'make': make,
#         # #             'family': family,
#         # #             'varient': varient,
#         # #             'new_pr': None,
#         # #             'nvic': nvic,
#         # #             'year': year,
#         # #
#         # #         }
#         # #         try:
#         # #             # print('23')
#         # #             task = DataUploadTask()
#         # #             task.run(**ctx)
#         # #
#         # #         except Exception as e:
#         # #             return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#         #
#         #
#         # return response.Response({"message": "successfully....."}, status=status.HTTP_201_CREATED)
#         # # return HttpResponse('successfully........')



# class DataUploadAPIView(generics.CreateAPIView):
#     """POST:API to send notifications"""
#
#     serializer_class = UploadSerializer
#
#     def post(self, request, *args, **kwargs):
#
#         from zipfile import ZipFile
#         import zipfile
#         from django.conf import settings
#         from PIL import Image
#         import glob
#         import os, os.path
#         import shutil
#         import pathlib
#         import pandas as pd
#         import pandas
#
#         import ftplib
#         from ftplib import FTP
#         global ftp
#
#         ftp = FTP('ftp.glassguide.com.au', user='dafs_ftp', passwd='Sefasob@17')
#
#         print(ftp.dir())
#         print(ftp.file)
#         ftp.cwd("/")
#
#         # filematch = '*Photo_Jan21.zip*'  # a match for any file in this case, can be changed or left for user to input
#         # filematch = '*Photo_Feb21.zip*'
#
#         # filematch = 'Apr21eis.zip'
#         # filematch = 'Feb21eis.zip'
#         filematch = '*eis.zip*'
#         # filematch = '*Photo*'
#
#
#         # # filename = 'Photo_Jan21.zip'
#         # # filename = 'Photo_Feb21.zip'
#
#         # filename = 'Apr21eis.zip'
#         # # # filename = 'Feb21eis.zip'
#         # # # filename = 'Mar21eis.zip'
#         # # filename = 'May21eis.zip'
#         # print(ftp.size(filename))
#
#
#         if not os.path.isfile('month_name_file.txt'):
#             my_file = open("month_name_file.txt", "a")
#
#         my_file = open("month_name_file.txt")
#         month_list = my_file.read()
#         # my_file.close()
#
#         for filename in ftp.nlst(filematch):
#             if filename.endswith('.zip'):
#                 month_name = filename.split('.')[0]
#                 # print('month_name',month_name)
#
#                 if month_name not in month_list:
#
#                     with open("month_name_file.txt", "a") as f:
#                         f.write("{0}\n".format(month_name))
#
#                     print('aaaaaaaaaaa')
#
#                     remove_path = str(settings.BASE_DIR) + "/data/"
#                     shutil.rmtree(remove_path, ignore_errors=True)
#
#                     remove_path = str(settings.BASE_DIR) + "/download/"
#                     shutil.rmtree(remove_path, ignore_errors=True)
#
#                     file = pathlib.Path(str(settings.BASE_DIR) + '/download/')
#                     if not file.exists():
#                         os.mkdir(str(settings.BASE_DIR) + '/download/')
#                     try:
#                         ftp.retrbinary("RETR " + filename,
#                                        open(str(settings.BASE_DIR) + "/download/" + filename, 'wb').write, 22253015)
#
#                     except TypeError:
#                         pass
#                     # ftp.retrbinary("RETR " + filename, open(str(settings.BASE_DIR) + "/download/" +filename, 'wb').write,22253015)
#                     # ftp.quit()
#
#                     zf = ZipFile(str(settings.BASE_DIR) + "/download/" +filename, "r")
#                     file = filename.split('.')[0]
#                     zf.extractall(str(settings.BASE_DIR) + '/data/' + file)
#
#                     file = pathlib.Path(str(settings.BASE_DIR) + '/data/')
#                     if not file.exists():
#                         os.mkdir(str(settings.BASE_DIR) + '/data/')
#
#                     file_name = os.listdir(str(settings.BASE_DIR) + '/data/')
#                     fileList = os.listdir(str(settings.BASE_DIR) + '/data/'+ file_name[0])
#                     # photo_path = str(settings.BASE_DIR) + '/data/{0}'.format(file_name[0])
#                     file_temp =file_name[0]
#
#
#                     for i in fileList:
#                         # if i =='CCG18041.N12' or i.endswith('U12'):
#                         # if i == 'CCG18041.N12' or i.endswith('N12'):
#                         if i == 'CCG18041.N12':
#                             print(i)
#
#                             filename = '{0}/{1}'.format(file_temp,i)
#                             df = pd.read_fwf(str(settings.BASE_DIR) + '/data/' + filename)
#
#                             # remove_path = str(settings.BASE_DIR) + "/csv_file/"
#                             # shutil.rmtree(remove_path, ignore_errors=True)
#                             file = pathlib.Path(str(settings.BASE_DIR) + '/csv_file/')
#                             if not file.exists():
#                                 os.mkdir(str(settings.BASE_DIR) + '/csv_file/')
#                             file = 'csv_file.csv'
#
#                             df.to_csv(str(settings.BASE_DIR) + '/csv_file/' + file)
#
#                             df = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                              usecols=lambda c: c in {'MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR'},
#                                              # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
#                                              # error_bad_lines=False,
#                                              dtype='unicode'
#                                              )
#                             make_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                                       usecols=lambda c: c in {'MAKE', },
#                                                       dtype='unicode'
#                                                       )
#                             family_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                                         usecols=lambda c: c in {'FAMILY', },
#                                                         dtype='unicode'
#                                                         )
#                             variant_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                                          usecols=lambda c: c in {'VARIANT', },
#                                                          dtype='unicode'
#                                                          )
#                             new_price_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                                            usecols=lambda c: c in {'NEW PR', },
#                                                            dtype='unicode'
#                                                            )
#                             nvic_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                                       usecols=lambda c: c in {'NVIC', },
#                                                       dtype='unicode'
#                                                       )
#                             year_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
#                                                       usecols=lambda c: c in {'YEAR', },
#                                                       dtype='unicode'
#                                                       )
#                             data = pandas.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file, dtype='unicode')
#                             column_len = len(df.columns)
#                             print('column_len',column_len)
#
#                             for i in range(len(data)):
#                                 if i == 0 or i == 1:
#                                     continue
#                                 if column_len == 5:
#                                     make = make_column.values[i][0]
#                                     family = family_column.values[i][0]
#                                     varient = variant_column.values[i][0]
#                                     nvic = nvic_column.values[i][0]
#                                     year = year_column.values[i][0]
#                                     # print('make', make, 'family', family, 'varient', varient,'nvic', nvic, 'year', year)
#
#                                     ctx = {
#                                         'make': make,
#                                         'family': family,
#                                         'varient': varient,
#                                         'new_pr': None,
#                                         'nvic': nvic,
#                                         'year': year,
#
#                                     }
#
#                                     # try:
#                                     #     # print('24')
#                                     #     task = DataUploadTask()
#                                     #     task.run(**ctx)
#                                     #
#                                     # except Exception as e:
#                                     #     return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#
#                                 if column_len == 6:
#                                     make = make_column.values[i][0]
#                                     family = family_column.values[i][0]
#                                     varient = variant_column.values[i][0]
#                                     new_pr = new_price_column.values[i][0]
#                                     nvic = nvic_column.values[i][0]
#                                     year = year_column.values[i][0]
#
#                                     # make = df.values[i][0]
#                                     # family = df.values[i][1]
#                                     # varient = df.values[i][2]
#                                     # nvic = df.values[i][3]
#                                     # year = df.values[i][4]
#                                     # new_pr = df.values[i][5]
#                                     # print('make', make, 'family', family,'varient', varient,'new_pr', new_pr,'nvic', nvic, 'year', year)
#
#                                     ctx = {
#                                         'make': make,
#                                         'family': family,
#                                         'varient': varient,
#                                         'new_pr': new_pr,
#                                         'nvic': nvic,
#                                         'year': year,
#
#                                     }
#                                     # try:
#                                     #     # print('24')
#                                     #     task = DataUploadTask()
#                                     #     task.run(**ctx)
#                                     #
#                                     # except Exception as e:
#                                     #     return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
#                 else:
#                     print('file allready upload')
#
#         return HttpResponse('successfully........')


class DataUploadAPIView(generics.CreateAPIView):
    """POST:API to send notifications"""

    serializer_class = UploadSerializer

    def post(self, request, *args, **kwargs):
        # Data.objects.all().delete()

        from zipfile import ZipFile
        import zipfile
        from django.conf import settings
        from PIL import Image
        import glob
        import os, os.path
        import shutil
        import pathlib
        import pandas as pd
        import pandas

        import ftplib
        from ftplib import FTP
        global ftp

        ftp = FTP('ftp.glassguide.com.au', user='dafs_ftp', passwd='Sefasob@17')

        print(ftp.dir())
        print(ftp.file)
        ftp.cwd("/")

        # filematch = '*Photo_Jan21.zip*'  # a match for any file in this case, can be changed or left for user to input
        # filematch = '*Photo_Feb21.zip*'

        # filematch = 'Apr21eis.zip'
        # filematch = 'Feb21eis.zip'
        filematch = '*eis.zip*'
        # filematch = '*Photo*'


        # # filename = 'Photo_Jan21.zip'
        # # filename = 'Photo_Feb21.zip'

        # filename = 'Apr21eis.zip'
        # # # filename = 'Feb21eis.zip'
        # # # filename = 'Mar21eis.zip'
        # # filename = 'May21eis.zip'
        # print(ftp.size(filename))


        if not os.path.isfile('month_name_file.txt'):
            my_file = open("month_name_file.txt", "a")

        my_file = open("month_name_file.txt")
        month_list = my_file.read()
        # my_file.close()

        for filename in ftp.nlst(filematch):
            if filename.endswith('.zip'):
                month_name = filename.split('.')[0]
                # print('month_name',month_name)

                if month_name not in month_list:

                    with open("month_name_file.txt", "a") as f:
                        f.write("{0}\n".format(month_name))

                    print('aaaaaaaaaaa')

                    # remove_path = str(settings.BASE_DIR) + "/data/"
                    # shutil.rmtree(remove_path, ignore_errors=True)
                    #
                    # remove_path = str(settings.BASE_DIR) + "/download/"
                    # shutil.rmtree(remove_path, ignore_errors=True)
                    #
                    # file = pathlib.Path(str(settings.BASE_DIR) + '/download/')
                    # if not file.exists():
                    #     os.mkdir(str(settings.BASE_DIR) + '/download/')
                    # try:
                    #     ftp.retrbinary("RETR " + filename,
                    #                    open(str(settings.BASE_DIR) + "/download/" + filename, 'wb').write, 22253015)
                    #
                    # except TypeError:
                    #     pass
                    # # ftp.retrbinary("RETR " + filename, open(str(settings.BASE_DIR) + "/download/" +filename, 'wb').write,22253015)
                    # # ftp.quit()
                    #
                    # zf = ZipFile(str(settings.BASE_DIR) + "/download/" +filename, "r")
                    # file = filename.split('.')[0]
                    # zf.extractall(str(settings.BASE_DIR) + '/data/' + file)
                    #
                    # file = pathlib.Path(str(settings.BASE_DIR) + '/data/')
                    # if not file.exists():
                    #     os.mkdir(str(settings.BASE_DIR) + '/data/')

                    file_name = os.listdir(str(settings.BASE_DIR) + '/data/')
                    fileList = os.listdir(str(settings.BASE_DIR) + '/data/'+ file_name[0])
                    # photo_path = str(settings.BASE_DIR) + '/data/{0}'.format(file_name[0])
                    file_temp =file_name[0]


                    for i in fileList:
                        # if i =='CCG18041.N12' or i.endswith('U12'):
                        # if i == 'CCG18041.N12' or i.endswith('N12'):
                        # if i == 'CCG18041.N12':
                        if i == 'PVG14041.N12':
                            print(i)
                            filename = '{0}/{1}'.format(file_temp,i)
                            df = pd.read_fwf(str(settings.BASE_DIR) + '/data/' + filename)
                            # remove_path = str(settings.BASE_DIR) + "/csv_file/"
                            # shutil.rmtree(remove_path, ignore_errors=True)
                            file = pathlib.Path(str(settings.BASE_DIR) + '/csv_file/')
                            if not file.exists():
                                os.mkdir(str(settings.BASE_DIR) + '/csv_file/')
                            file = 'csv_file.csv'

                            df.to_csv(str(settings.BASE_DIR) + '/csv_file/' + file)

                            df = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                             usecols=lambda c: c in {'MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR'},
                                             # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
                                             # error_bad_lines=False,
                                             dtype='unicode'
                                             )
                            make_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                                      usecols=lambda c: c in {'MAKE', },
                                                      dtype='unicode'
                                                      )
                            family_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                                        usecols=lambda c: c in {'FAMILY', },
                                                        dtype='unicode'
                                                        )
                            variant_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                                         usecols=lambda c: c in {'VARIANT', },
                                                         dtype='unicode'
                                                         )
                            new_price_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                                           usecols=lambda c: c in {'NEW PR', },
                                                           dtype='unicode'
                                                           )
                            nvic_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                                      usecols=lambda c: c in {'NVIC', },
                                                      dtype='unicode'
                                                      )
                            year_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
                                                      usecols=lambda c: c in {'YEAR', },
                                                      dtype='unicode'
                                                      )
                            data = pandas.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file, dtype='unicode')
                            column_len = len(df.columns)
                            print('column_len',column_len)

                            nvics = Data.objects.all()
                            nvic_list = []
                            for nv in nvics:
                                nvic_list.append(nv.nvic)

                            insert_list = []
                            objs = []

                            for i in range(len(data)):
                                if i == 0 or i == 1:
                                    continue

                                if column_len == 5:
                                    make = make_column.values[i][0]
                                    family = family_column.values[i][0]
                                    varient = variant_column.values[i][0]
                                    nvic = nvic_column.values[i][0]
                                    year = year_column.values[i][0]
                                    # print('make', make, 'family', family, 'varient', varient,'nvic', nvic, 'year', year)

                                    ctx = {
                                        'make': make,
                                        'family': family,
                                        'varient': varient,
                                        'new_pr': None,
                                        'nvic': nvic,
                                        'year': year,

                                    }
                                    if nvic not in nvic_list:
                                        print('store intp data')

                                        insert_list.append(Data(make=make, family =family,varient=varient ,new_pr=ctx['new_pr'], nvic=nvic,year=year))

                                    else:
                                        print('update')
                                        obj = Data.objects.get(nvic=nvic)
                                        obj.make = make
                                        obj.family = family
                                        obj.varient = varient
                                        obj.new_pr = ctx['new_pr']
                                        obj.year = year
                                        objs.append(obj)

                                if column_len == 6:
                                    make = make_column.values[i][0]
                                    family = family_column.values[i][0]
                                    varient = variant_column.values[i][0]
                                    new_pr = new_price_column.values[i][0]
                                    nvic = nvic_column.values[i][0]
                                    year = year_column.values[i][0]
                                    # print('make', make, 'family', family, 'varient', varient,'nvic', nvic, 'year', year, 'new_pr',new_pr)

                                    ctx = {
                                        'make': make,
                                        'family': family,
                                        'varient': varient,
                                        'new_pr': new_pr,
                                        'nvic': nvic,
                                        'year': year,

                                    }

                                    if nvic not in nvic_list:
                                        print('store intp data')
                                        insert_list.append(Data(make=make, family =family,varient=varient ,new_pr=new_pr, nvic=nvic,year=year))

                                    else:
                                        print('update')
                                        obj = Data.objects.get(nvic=nvic)
                                        obj.make = make
                                        obj.family = family
                                        obj.varient = varient
                                        obj.new_pr = new_pr
                                        obj.year = year
                                        objs.append(obj)

                            Data.objects.bulk_update(objs, ['make', 'family', 'varient', 'new_pr', 'year'])
                            Data.objects.bulk_create(insert_list)

                            print('bulk_updated',objs)
                            print('bulk_created',insert_list)


                else:
                    print('file allready upload')

        return HttpResponse('successfully........')



def snippet_detail(request):

    from .models import Csv_Data

    # insert_list = []
    # for i in range(1):
    #     month_name = 'month_name'
    #     is_active = False
    #     insert_list.append(Csv_Data(month_name=month_name, is_active =is_active))
    # Csv_Data.objects.bulk_create(insert_list)


    objs = []
    for i in range(1):
        month_name = 'month_name'
        is_active = True
        obj = Csv_Data.objects.get(month_name=month_name)
        obj.is_active = is_active
        obj.month_name = 'aaaaaaaa'
        objs.append(obj)
    Csv_Data.objects.bulk_update(objs, ['is_active','month_name'])

    name = Csv_Data.objects.all()
    for i in name:
        print('asasasa', i.is_active)
        print(i)



    # import schedule
    # import time
    # from datetime import date
    # from datetime import datetime
    #
    # now = datetime.now()
    # print(now)
    #
    # def job():
    #
    #     if date.today().day == 22:
    #         print("I'm working...")
    #     if date.today().day == 21:
    #
    #         print("I'm 21")
    #         from zipfile import ZipFile
    #         import zipfile
    #         from django.conf import settings
    #         from PIL import Image
    #         import glob
    #         import os, os.path
    #         import shutil
    #         import pathlib
    #         import pandas as pd
    #         import pandas
    #
    #         import ftplib
    #         from ftplib import FTP
    #         global ftp
    #
    #         ftp = FTP('ftp.glassguide.com.au', user='dafs_ftp', passwd='Sefasob@17')
    #
    #         print(ftp.dir())
    #         print(ftp.file)
    #
    #         # filematch = '*Photo_Jan21.zip*'  # a match for any file in this case, can be changed or left for user to input
    #         # filematch = '*Photo_Feb21.zip*'
    #         # filematch = 'Apr21eis.zip'
    #         # filematch = '*eis.zip*'
    #         # filematch = '*Photo*'
    #
    #         ftp.cwd("/")
    #
    #
    #         # # filename = 'Photo_Jan21.zip'
    #         # # filename = 'Photo_Feb21.zip'
    #
    #         filename = 'Apr21eis.zip'
    #         # # filename = 'Feb21eis.zip'
    #         # # filename = 'Mar21eis.zip'
    #         # filename = 'May21eis.zip'
    #         print(ftp.size(filename))
    #
    #         remove_path = str(settings.BASE_DIR) + "/data/"
    #         shutil.rmtree(remove_path, ignore_errors=True)
    #
    #         remove_path = str(settings.BASE_DIR) + "/download/"
    #         shutil.rmtree(remove_path, ignore_errors=True)
    #
    #         file = pathlib.Path(str(settings.BASE_DIR) + '/download/')
    #         if not file.exists():
    #             os.mkdir(str(settings.BASE_DIR) + '/download/')
    #
    #         ftp.retrbinary("RETR " + filename, open(str(settings.BASE_DIR) + "/download/" +filename, 'wb').write,22253015)
    #         ftp.quit()
    #
    #         zf = ZipFile(str(settings.BASE_DIR) + "/download/" +filename, "r")
    #         file = filename.split('.')[0]
    #         zf.extractall(str(settings.BASE_DIR) + '/data/' + file)
    #
    #         file = pathlib.Path(str(settings.BASE_DIR) + '/data/')
    #         if not file.exists():
    #             os.mkdir(str(settings.BASE_DIR) + '/data/')
    #
    #         file_name = os.listdir(str(settings.BASE_DIR) + '/data/')
    #         fileList = os.listdir(str(settings.BASE_DIR) + '/data/'+ file_name[0])
    #         photo_path = str(settings.BASE_DIR) + '/data/{0}'.format(file_name[0])
    #         file_temp =file_name[0]
    #
    #
    #         for i in fileList:
    #             if i =='CCG18041.N12':
    #             # if i == 'CCG18041.N12' or i.endswith('N12'):
    #             # if i == 'CCG18051.U12':
    #                 filename = '{0}/{1}'.format(file_temp,i)
    #                 df = pd.read_fwf(str(settings.BASE_DIR) + '/data/' + filename)
    #
    #                 # remove_path = str(settings.BASE_DIR) + "/csv_file/"
    #                 # shutil.rmtree(remove_path, ignore_errors=True)
    #
    #                 file = pathlib.Path(str(settings.BASE_DIR) + '/csv_file/')
    #                 if not file.exists():
    #                     os.mkdir(str(settings.BASE_DIR) + '/csv_file/')
    #                 file = 'csv_file.csv'
    #
    #                 df.to_csv(str(settings.BASE_DIR) + '/csv_file/' + file)
    #
    #
    #                 df = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                  usecols=lambda c: c in {'MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR'},
    #                                  # usecols =['MAKE', 'FAMILY', 'VARIANT', 'NEW PR', 'NVIC', 'YEAR']
    #                                  # error_bad_lines=False,
    #                                  dtype='unicode'
    #                                  )
    #
    #                 make_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                           usecols=lambda c: c in {'MAKE', },
    #                                           dtype='unicode'
    #                                           )
    #
    #                 family_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                             usecols=lambda c: c in {'FAMILY', },
    #                                             dtype='unicode'
    #                                             )
    #                 variant_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                              usecols=lambda c: c in {'VARIANT', },
    #                                              dtype='unicode'
    #                                              )
    #                 new_price_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                                usecols=lambda c: c in {'NEW PR', },
    #                                                dtype='unicode'
    #                                                )
    #
    #                 nvic_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                           usecols=lambda c: c in {'NVIC', },
    #                                           dtype='unicode'
    #                                           )
    #
    #                 year_column = pd.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file,
    #                                           usecols=lambda c: c in {'YEAR', },
    #                                           dtype='unicode'
    #                                           )
    #
    #                 data = pandas.read_csv(str(settings.BASE_DIR) + '/csv_file/' + file, dtype='unicode')
    #                 column_len = len(df.columns)
    #                 print('column_len',column_len)
    #
    #                 for i in range(len(data)):
    #                     if i == 0 or i == 1:
    #                         continue
    #                     if column_len == 5:
    #                         make = make_column.values[i][0]
    #                         family = family_column.values[i][0]
    #                         varient = variant_column.values[i][0]
    #                         nvic = nvic_column.values[i][0]
    #                         year = year_column.values[i][0]
    #                         print('make', make, 'family', family, 'varient', varient,'nvic', nvic, 'year', year)
    #
    #                         ctx = {
    #                             'make': make,
    #                             'family': family,
    #                             'varient': varient,
    #                             'new_pr': None,
    #                             'nvic': nvic,
    #                             'year': year,
    #
    #                         }
    #
    #                         # try:
    #                         #     # print('24')
    #                         #     task = DataUploadTask()
    #                         #     task.run(**ctx)
    #                         #
    #                         # except Exception as e:
    #                         #     return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    #
    #                     if column_len == 6:
    #                         make = make_column.values[i][0]
    #                         family = family_column.values[i][0]
    #                         varient = variant_column.values[i][0]
    #                         new_pr = new_price_column.values[i][0]
    #                         nvic = nvic_column.values[i][0]
    #                         year = year_column.values[i][0]
    #
    #                         # make = df.values[i][0]
    #                         # family = df.values[i][1]
    #                         # varient = df.values[i][2]
    #                         # nvic = df.values[i][3]
    #                         # year = df.values[i][4]
    #                         # new_pr = df.values[i][5]
    #                         print('make', make, 'family', family,'varient', varient,'new_pr', new_pr,'nvic', nvic, 'year', year)
    #
    #                         ctx = {
    #                             'make': make,
    #                             'family': family,
    #                             'varient': varient,
    #                             'new_pr': new_pr,
    #                             'nvic': nvic,
    #                             'year': year,
    #
    #                         }
    #                         # try:
    #                         #     # print('24')
    #                         #     task = DataUploadTask()
    #                         #     task.run(**ctx)
    #                         #
    #                         # except Exception as e:
    #                         #     return response.Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    #
    #
    # # # schedule.every(2).seconds.do(job)
    # # # schedule.every().hour.do(job)
    # schedule.every().day.at("15:09").do(job)
    # # schedule.every().monday.do(job)
    # # schedule.every().wednesday.at("13:15").do(job)
    # # schedule.every().minute.at(":17").do(job)

    # while True:
    #     schedule.run_pending()
    #     time.sleep(1)









    # import sys
    # import json
    #
    # fil = open("/home/jaarvis/Downloads/sample.json")
    # lis = fil.readlines()
    #
    # f = open("/home/jaarvis/Downloads/sample.json")
    # x = json.load(f)
    # print('aaaaaaaaaaaaaaaa',f)
    # # f = csv.writer(open("/home/jaarvis/Downloads/samplea.csv", "wb+"))
    #
    # # Write CSV Header, If you dont need that, remove this line
    # # f.writerow(["pk", "model", "codename", "name"])
    # #
    # # for x in x:
    # #     f.writerow([x["pk"],
    # #                 x["model"],
    # #                 x["fields"]["codename"],
    # #                 # x["fields"]["name"],
    #                 # x["fields"]["content_type"]
    #                 # ])
    #
    #
    # # f = open('/home/jaarvis/Downloads/tsdsd.csv')
    # # csv_file = csv.writer(f)
    # # for item in data:
    # #     csv_file.writerow(item)
    # # print('aaaaaaaaaaaaaaaa')
    # # f.close()
    #
    #
    # import pdfkit
    # pdfkit.from_file('/home/jaarvis/Downloads/samplea.csv',
    #                  '/home/jaarvis/Downloads/samples.pdf')
    # Data.objects.all().delete()
    # admin = User.objects.create_superuser(email='superadmin@gmail.com',
    # mobile_number="8930337370",
    # password='Qwerty@12345')
    # admin.save()
    # #     return redirect('http://165.22.99.208:8080/dashboard')
    # return HttpResponse('Super user create succesfully.....')



class CompanyAddressCheckView(generics.CreateAPIView):
    serializer_class = CompanyAddressCheckSerializers

    def create(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            import datetime

            if 'abn_number' in request.data:
                abn_number = request.data.get("abn_number")

            if 'serach_by' in request.data:
                serach_by = request.data.get("serach_by")
            if 'serach_by' not in request.data:
                serach_by =None

            import requests

            url = "https://hosted.mastersoftgroup.com/harmony/rest/AU/locality"

            # querystring = {"featureOptions": "caseType:TITLE;exposeAttributes:1", "locality": locality,
            # # querystring = {"featureOptions": "caseType:TITLE;exposeAttributes:1",
            #                "sourceOfTruth": "AUPAF"}
            postcode = 3004
            # locality = 'Melbourne'
            fullAddress = None
            locality = None
            querystring = {"featureOptions": "caseType:TITLE;exposeAttributes:1", "locality": locality,
                           "fullAddress": fullAddress,
                           "postcode": postcode, "sourceOfTruth": "AUPAF"}

            # querystring = {"featureOptions": "caseType:TITLE;exposeAttributes:1", "fullAddress": "Mel",
            #                "postcode": "3000", "sourceOfTruth": "AUPAF"}

            headers = {
                'authorization': "Basic ZGlnbHRkX3Rlc3R1c2VyOklUWkRPaWRtSlpUdERMakRkc3dkYVNiaHhPODZnUnhH",
                'cache-control': "no-cache",
            }

            response = requests.request("GET", url, headers=headers, params=querystring)
            if response.status_code == 200:
                # print(response.json())
                data = response.json()
                aa = data['payload']

                # for i in aa:
                #     print(i['_type'])
                #     print(i['postcode'])
                #     print(i['postal'])

                dict_data = {}
                ss = {'payload': aa}
                dict_data.update(ss)
                return Response(
                    data,
                    # aa,
                    # dict_data,
                    status=status.HTTP_201_CREATED,
                    )
            else:
                raise exceptions.ValidationError("User Unauthorized")
















            # data_obj = CompanyAddress.objects.filter(abn_number=abn_number).first()
            # if data_obj:
            #     # api_hit_date = CompanyAddress.api_hit_date
            #     # now = datetime.datetime.now()
            #     # d0 = api_hit_date.date()
            #     # d1 = now.date()
            #     # delta = d1 - d0
            #     # days = delta.days
            #     # print(days)
            #     # if days < 90:
            #     #     print('data from db')
            #     #     now = datetime.datetime.now()
            #     #     data_obj.modified_on = now
            #     #     data_obj.save()
            #
            #     dict_data = data_obj.company_data
            #     print('data from db')
            #     return Response(
            #         dict_data,
            #         status=status.HTTP_201_CREATED,
            #     )
            #
            #
            # import requests
            #
            # url = "https://hosted.mastersoftgroup.com/harmony/rest/AU/companyLookup"
            #
            # # querystring = {"apiName": "ABRSearchByABN", "name": abn_number}
            # # print(serach_by)
            # querystring = {"apiName": serach_by, "name": abn_number}
            #
            # headers = {
            #     'authorization': "Basic ZGlnbHRkX3Rlc3R1c2VyOklUWkRPaWRtSlpUdERMakRkc3dkYVNiaHhPODZnUnhH",
            #     'cache-control': "no-cache",
            # }
            #
            # response = requests.request("GET", url, headers=headers, params=querystring)
            # data = response.text
            # # print(data)
            # aa = data.split(',')
            # dict_data = {}
            # if "name" in data:
            #     for id, value in enumerate(aa):
            #         if id == 2:
            #             name = value.split(':')[2]
            #             name = name.replace('"', '')
            #             ss = {'name': name}
            #             dict_data.update(ss)
            #             # print('name', name)
            #
            #         if id == 3:
            #             abn = value.split(':')[1]
            #             abn = abn.replace('"', '')
            #             ss = {'abn': abn}
            #             dict_data.update(ss)
            #             # print('abn',abn)
            #
            #         if id == 4:
            #             statecode = value.split(':')[1]
            #             statecode = statecode.replace('"', '')
            #             ss = {'statecode': statecode}
            #             dict_data.update(ss)
            #             # print('statecode',statecode)
            #         if id == 5:
            #             postcode = value.split(':')[1]
            #             postcode = postcode.replace('"', '')
            #             ss = {'postcode': postcode}
            #             dict_data.update(ss)
            #             # print('postcode',postcode)
            #         if id == 6:
            #             statuse = value.split(':')[1]
            #             statuse = statuse.replace('"', '')
            #             ss = {'statuse': statuse}
            #             dict_data.update(ss)
            #             # print('statuse',statuse)
            #         if id == 7:
            #             legalName = value.split(':')[1]
            #             legalName = legalName.replace('"', '')
            #             ss = {'legalName': legalName}
            #             dict_data.update(ss)
            #             # print('legalName', legalName)
            #         if id == 8:
            #             acn = value.split(':')[1]
            #             acn = acn.replace('"', '')
            #             ss = {'acn': acn}
            #             dict_data.update(ss)
            #             # print('acn', acn)
            #         if id == 9:
            #             isCurrentIndicator = value.split(':')[1]
            #             isCurrentIndicator = isCurrentIndicator.replace('"', '')
            #             ss = {'isCurrentIndicator': isCurrentIndicator}
            #             dict_data.update(ss)
            #             # print('isCurrentIndicator', isCurrentIndicator)
            #         if id == 10:
            #             entityDescription = value.split(':')[1]
            #             entityDescription = entityDescription.replace('"', '')
            #             ss = {'entityDescription': entityDescription}
            #             dict_data.update(ss)
            #             # print('entityDescription', entityDescription)
            #     print('api hit first time push into db')
            #     now = datetime.datetime.now()
            #     print('aaaaaaaaaaaaaa')
            #     CompanyAddress.objects.create(abn_number=abn_number, company_data=dict_data, api_hit_date=now)
            #
            #     return Response(
            #         dict_data,
            #         status=status.HTTP_201_CREATED,
            #     )
            # else:
            #     raise exceptions.ValidationError("Company address doesn't found")




# def snippet_detail(request):
#     from django.http import HttpResponse
#     import schedule
#
#     def job():
#         print("I'm working...")
#
#         from datetime import datetime, timezone
#         now = datetime.now(timezone.utc)
#         # now = datetime.now()
#         print('now', now)
#
#         application_obj = Application.objects.all()
#         for i in application_obj:
#             application_created_on = i.created_on
#             if application_created_on:
#                 minute = now - application_created_on
#                 # minute = booking_date - now
#                 minutes = minute.total_seconds() / 60
#                 status = i.status
#                 # print('minutes', minutes)
#                 # print(status)
#                 if status == '2':
#                     if minutes > 1440:
#                         # if minutes < 110:
#                         # i.created_on =now
#                         # i.save()
#                         print('aaaaaaaaaaaaaaaaa')
#                         customer_id = i.customer_id
#                         dealer_id = i.dealer_id
#                         agent_id = i.agent_id
#                         application_id = i.id
#                         application_name = i.application_name
#                         # if customer_id:
#                         #     customer_data = notification_req_app_handler_user.get_user_detail(customer_id)
#                         #     id = customer_data['id']
#                         #     customer_email = customer_data['email']
#                         #     first_name = customer_data['first_name']
#                         #     last_name = customer_data['last_name']
#
#     schedule.every(10).seconds.do(job)
#     # schedule.every(1).minutes.do(job)
#
#     while True:
#         schedule.run_pending()
#     #     time.sleep(1)
#     return HttpResponse('Super user create succesfully.....')
