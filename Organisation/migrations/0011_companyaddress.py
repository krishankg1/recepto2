# Generated by Django 3.2 on 2021-07-02 08:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Organisation', '0010_places_company_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyAddress',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('abn', models.CharField(max_length=200, unique=True)),
                ('company_data', models.JSONField(blank=True, null=True)),
            ],
        ),
    ]
