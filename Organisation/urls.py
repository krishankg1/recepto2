# from django.urls import path
from .views import accountsViewset, PlaceViewset, LoginView
#
# from rest_framework.routers import DefaultRouter
#
# router = DefaultRouter()
# router.register(r'user', accountsViewset, basename='user')
# router.register(r'places', PlaceViewset, basename='places')
#
#
# urlpatterns = [
#     path('login/', LoginView.as_view(), name='login'),
# ]+router.urls


from django.conf.urls import url
from rest_framework import routers

from . import views




from django.urls import path
from django.conf.urls import url, include
# from . import apis

# from .views import (SocialLoginView)
from rest_framework.routers import DefaultRouter
router = DefaultRouter()


# router = routers.SimpleRouter()
router.register(r'user', views.accountsViewset, basename='user')
router.register(r'places', views.PlaceViewset, basename='places')
router.register(r'project', views.ProjectViewSet, basename='project')
router.register(r'lot', views.LottViewSet, basename='lot')

# router.register(r'receptionist', views.ReceptionistViewSet, basename='receptionist')

urlpatterns = [
    url(r'^', include(router.urls)),

    url('login/', views.LoginView.as_view(), name='login'),
    url(r'data-upload/$', views.DataUploadAPIView.as_view(), name='data-upload'),
    url(r'^createsuperuser/$', views.snippet_detail, name='createsuperuser'),

    # url(r'^login/$', views.LoginView.as_view(), name='user-login'),
    # url(r'^logout/$', views.LogoutView.as_view(), name='user-logout'),
    # url(r'^changepassword/$', views.ChangePasswordView.as_view(),name='change-password'),
    # url(r'^forgotpassword/$', views.ForgotPasswordView.as_view(),name='forgot-password'),
    #
    # url(r'^createsuperuser/$', views.snippet_detail, name='createsuperuser'),
    url(r'^company-address-check-api/$', views.CompanyAddressCheckView.as_view(),
        name="company-address-check"),


]
