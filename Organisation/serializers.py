from rest_framework import serializers
from .models import Places, accounts
from .models import Project
from .models import Csv_Data
from .models import Lot
from django.contrib import messages
from django.contrib.auth import authenticate




class accountsSerializer(serializers.ModelSerializer):
    class Meta:
        model = accounts
        fields = '__all__'


class acountsSerializercreate(serializers.ModelSerializer):
    confirm_password = serializers.CharField(style={'input_type': 'password'}, write_only=True)
     #email = serializers.EmailField(write_only=True)

    #places = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault)
    class Meta:
        model = accounts
        fields = ['email', 'password', 'confirm_password', 'organization', 'username', 'domain']
        extra_kwargs = {
                'password': {'write_only': True}
        }
    def save(self):
        account = accounts(
            email=self.validated_data['email'],
            username=self.validated_data['username'],
            organization=self.validated_data['organization'],
            domain=self.validated_data['email'].split('@')[1],



        )
        password = self.validated_data['password']
        confirm_password = self.validated_data['confirm_password']



        if password != confirm_password:
            raise serializers.ValidationError({'password': 'password must match'})
        else:

            account.set_password(password)
            account.save()


            return account

class accountsSerializerupdate(serializers.ModelSerializer):
    class Meta:
        model = accounts
        fields = ['email', 'password', 'organization', 'username']







class PlacesSerializer(serializers.ModelSerializer):
    places = serializers.HiddenField(default=serializers.CurrentUserDefault)
    class Meta:
        model = Places
        fields = ['username', 'organization', 'placename', 'distt', 'state', 'places', 'id', 'qr_code']

class PlacesSerializercreate(serializers.ModelSerializer):
    class Meta:
        model = Places
        fields = '__all__'
class PlacesSerializerupdate(serializers.ModelSerializer):
    class Meta:
        model = Places
        fields = '__all__'






from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings



JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER

class loginserializer(serializers.Serializer):

    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        email = data.get("email", None)
        password = data.get("password", None)
        user = authenticate(email=email, password=password)
        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )
        try:
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            update_last_login(None, user)
        except accounts.DoesNotExist:
            raise serializers.ValidationError(
                'User with given email and password does not exists'
            )
        return {
            'email': user.email,
            'token': jwt_token
        }

class ProjectTypeListSerializers(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = "__all__"


class LotTypeListSerializers(serializers.ModelSerializer):
    project_id = serializers.SerializerMethodField()
    # project_id = ProjectTypeListSerializers(many=True)

    def get_project_id(self, obj):
        name = []
        if obj.project_id:
            project_id = obj.project_id.id
            mobile_number = obj.project_id.project_name
            data = {
                "id": project_id,
                "project_name": mobile_number,

                # "document_id": document_obj.id if document_obj else None
            }
            name.append(data)
        return name

    class Meta:
        model = Lot
        fields = "__all__"


class LotCreateSerializer(serializers.ModelSerializer):

    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = Lot
        fields = ("lot_name", "is_active", "project_id", )

class UploadSerializer(serializers.Serializer):
    csv_file = serializers.FileField()
    # csv_file = serializers.ImageField()
    # model = serializers.CharField(max_length=10, required=True)


class CompanyAddressCheckSerializers(serializers.ModelSerializer):

    """
        serializer for registering new user
    """
    abn_number = serializers.CharField(
        min_length=2,
        max_length=100,
        required=False
    )
    serach_by = serializers.CharField(
        min_length=2,
        max_length=100,
        required=False
    )

    class Meta:
        model = Csv_Data
        fields = (
            'abn_number',
            'serach_by'
            # 'last_name',
            # # 'middle_name',
            # 'dob',
            # 'suburb', 'post_code', 'state',
            # 'gender',
            # 'user_id',
            # 'licence_number',
            # 'account_type'
        )
        extra_kwargs = {
                        # 'middle_name': {'required': False},
                        # 'mobile_number': {'required': True},
                        'abn_number': {'required': True},
                        # 'last_name': {'required': True},
                        # 'dob': {'required': True},
                        # 'suburb': {'required': True},
                        # 'post_code': {'required': True},
                        # 'state': {'required': True},
                        # 'gender': {'required': True},
                        # 'licence_number': {'required': True},
                        # 'user_id': {'required': False},
                        }



