from django.db import models
from Organisation.models import Places
import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw

class Visitrecord(models.Model):

    visname = models.CharField(max_length=40)
    visemail = models.EmailField(max_length=40)
    vispurp = models.CharField(max_length=40)
    vismob = models.CharField(max_length=40)
    authemail = models.EmailField(max_length=30)
    visdomain = models.CharField(max_length=30, default=' ')
    place = models.ForeignKey(Places, to_field="placename", related_name="location", on_delete=models.DO_NOTHING, null=True, blank=True,)
    qr_code = models.ImageField(upload_to='qr_code', blank=True)



    def __str__(self):
        return self.visname

    @property
    def combine(self):
        return '{}-{}-{}-{}-{}'.format(self.visname, self.vispurp, self.place, self.vismob , self.authemail)

    url = combine

    def save(self, *args, **kwargs):
        qrcode_img = qrcode.make(self.url)
        canvas = Image.new('RGB', (450, 450), 'white')
        draw = ImageDraw.Draw(canvas)
        canvas.paste(qrcode_img)
        fname = f'qr_code-{self.visname}.png'
        buffer = BytesIO()
        canvas.save(buffer, 'PNG')
        self.qr_code.save(fname, File(buffer), save=False)
        canvas.close()
        super().save(*args, **kwargs)

class VisitLogBook(models.Model):
    Wait_status = 'Waiting'
    Current_status = 'In Arena'
    Over_status = 'overtime'
    complete_status = 'Visit Complete'
    STATUS_CHOICES = [
        (Wait_status, 'Waiting'),
        (Current_status, 'In Arena'),
        (Over_status, 'overtime'),
        (complete_status, 'Visit Complete')
        ]

    visitor = models.OneToOneField(Visitrecord, related_name='visitor', on_delete=models.CASCADE, default='2', null=True, blank=True)
    Status = models.CharField(max_length=40, choices=STATUS_CHOICES, default=" IDLE ")
    Time_in = models.DateTimeField(verbose_name='Time_in', null=True, auto_now_add=True)
    Time_out = models.DateTimeField(verbose_name='Time_out', null=True,  auto_now_add=False)
    In_campus = models.BooleanField(default=False)
    otp = models.CharField(max_length=20, null=True, blank=True)


    def __str__(self):
        return self.visitor.visname