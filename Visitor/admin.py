from django.contrib import admin
from . models import Visitrecord, VisitLogBook


admin.site.register(Visitrecord)
admin.site.register(VisitLogBook)
