# Generated by Django 3.2 on 2021-04-14 01:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Organisation', '0003_alter_places_placename'),
        ('Visitor', '0002_auto_20210414_0621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visitrecord',
            name='place',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='location', to='Organisation.places', to_field='placename'),
        ),
    ]
