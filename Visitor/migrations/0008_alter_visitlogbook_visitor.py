# Generated by Django 3.2 on 2021-04-15 09:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Visitor', '0007_alter_visitlogbook_time_in'),
    ]

    operations = [
        migrations.AlterField(
            model_name='visitlogbook',
            name='visitor',
            field=models.OneToOneField(blank=True, default='2', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='visitor', to='Visitor.visitrecord'),
        ),
    ]
