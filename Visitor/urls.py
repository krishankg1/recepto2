from django.urls import path
from .viewsets import VisitecordViewset, VisitLogBookViewset
from .views import SendOTP, ValidateOTP

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'visitor', VisitecordViewset, basename='user')
router.register(r'visitorlog', VisitLogBookViewset, basename='visitorlog')



urlpatterns = [
    path('sendotp/', SendOTP.as_view(), name='sendotp'),
    path('verifyotp/', ValidateOTP.as_view(), name='verifyotp'),
]+router.urls





#from Visitor.viewsets import VisitecordViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('visitor', VisitecordViewset)