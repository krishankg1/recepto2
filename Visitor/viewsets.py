from rest_framework import viewsets
from . import models
from . import serializers
from . serializers import VisitrecordcreateSerializer, VisitLogBookSerializer

class VisitecordViewset(viewsets.ModelViewSet):
    queryset = models.Visitrecord.objects.all()
    serializer_class = serializers.VisitrecordSerializer

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return VisitrecordcreateSerializer

class VisitLogBookViewset(viewsets.ModelViewSet):
    queryset = models.VisitLogBook.objects.all()
    serializer_class = serializers.VisitLogBookSerializer

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return VisitLogBookSerializer