from rest_framework import serializers
from .models import Visitrecord, VisitLogBook
from Organisation.models import accounts, Places
from django.conf import settings
from django.core.mail import send_mail


class VisitrecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitrecord
        fields = '__all__'

class VisitrecordcreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitrecord
        fields = '__all__'

    def save(self):
        visit = Visitrecord(
        visname=self.validated_data['visname'],
        visemail=self.validated_data['visemail'],
        vispurp=self.validated_data['vispurp'],
        vismob=self.validated_data['vismob'],
        authemail=self.validated_data['authemail'],
        visdomain=self.validated_data['authemail'].split('@')[1],
        place=self.validated_data['place']

        )
        visdomain = self.validated_data['authemail'].split('@')[1]
        dom = list(accounts.objects.values_list('email', flat=True))
        dom2 = (','.join(dom))
        if visdomain not in dom2:
            raise serializers.ValidationError({'error': 'incorrect domain'})

        else:
            subject = 'NEW VISITOR'
            message = f'NAME:= {visit.visname} || PURPOSE:= {visit.vispurp} || MOBILE NUMBER := {visit.visemail} || LINK TO GENERATE OTP:="http://127.0.0.1:8000/visitor/sendotp/ '
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [visit.authemail, ]
            send_mail(subject, message, email_from, recipient_list)

            visit.save()

            return visit


class VisitLogBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisitLogBook
        fields = ['visitor', 'Status', 'Time_in', 'In_campus', 'Time_out']

        def save(self):
            log =VisitLogBook(
                visitor=self.validated_data['visitor']
            )

            log.save()
            return log